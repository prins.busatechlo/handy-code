const jwt = require("jsonwebtoken");
const secretKey = "WOLF";

exports.protectRoute = async (req, res, next) => {
  try {
    // const authHeader = req.headers["authorization"];
    // const token = authHeader && authHeader.split(" ")[1];
    const { token } = req.body;
    console.log(token);
    if (token == null) return res.sendStatus(401);
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(401).send({ message: "Invalid token" });
      }

      // req.userId = decoded.userId; // Add the user ID to the request object
      next();
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: "Internal server error" });
  }
};
