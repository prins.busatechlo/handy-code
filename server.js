const express = require("express");
const app = express();
const mongoose = require("mongoose");
const port = process.env.port || 5000;
const route = require("./routes/route.js");

app.use(express.json());
app.use(route);

mongoose
  .connect("mongodb://127.0.0.1:27017/auth-users", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("DB connection is sucessful");
  })
  .catch((err) => {
    console.log("no connection");
  });

app.listen(port, () => {
  console.log(`connection is sucessful ${port}`);
});
