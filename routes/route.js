const express = require("express");
const router = new express.Router();
const {
  singup,
  login,
  protected,
  home,
  forgotpassword,
  resetpassword,
} = require("../controller/userauth");

const { protectRoute } = require("../middlewae/auth");
//Auth

router.post("/register", singup);
router.post("/login", login);
router.get("/home", home);
router.get("/protected", protectRoute, protected);
router.post("/forgotpassword", forgotpassword);
router.post("/reset-password/:token", resetpassword);

module.exports = router;
